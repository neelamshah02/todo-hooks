import React, { useState } from "react";
import TodoForm from "./components/TodoForm";
import TodoList from "./components/TodoList";

import "./App.css";

const App = () => {
  const [todos, setTodos] = useState([]);
  return (
    <div className="App">
      <div className="App-header">To-Do</div>
      <TodoForm
        saveTodo={todo => {
          setTodos([...todos, todo]);
        }}
      />
      <TodoList
        todos={todos}
        deleteTodo={todoIndex => {
          const newTodos = todos.filter((_, index) => index !== todoIndex);
          setTodos(newTodos);
        }}
        doneTodo={todoIndex => {
          let updatedItems = todos.map((item, key) => {
            if (todoIndex === key) {
              item.done = !item.done;
            }
            return item;
          });
          setTodos(updatedItems);
        }}
      />
    </div>
  );
};

export default App;
