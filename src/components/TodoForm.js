import React, { useState } from "react";
import "./TodoForm.css";

const TodoForm = ({ saveTodo }) => {
  const [text, setText] = useState("");
  const onChangeInput = e => {
    setText(e.target.value);
  };

  const onSave = e => {
    e.preventDefault();

    let newTask = {
      id: Date.now(),
      task: text,
      done: false,
      color: "green"
    };
    saveTodo(newTask);
    setText("");
  };
  return (
    <form>
      <input
        className="todo-input"
        type="text"
        placeholder="Add todo"
        value={text}
        onChange={onChangeInput}
      />
      <button onClick={onSave}>add</button>
    </form>
  );
};
export default TodoForm;
