import React from "react";
import { FaTrash } from "react-icons/fa";
import "./TodoList.css";

const TodoList = ({ todos, deleteTodo, doneTodo }) => {
  return (
    <ul>
      {todos.map((todo, index) => (
        <li className="todo-each" key={index}>
          <input
            className="check-icon"
            type="checkbox"
            id="check"
            name="check"
            onClick={() => doneTodo(index)}
          />
          <span className={todo.done ? "done" : "not-done"}>{todo.task}</span>
          <FaTrash
            className="delete-icon"
            aria-label="Delete"
            onClick={() => {
              deleteTodo(index);
            }}
            aria-hidden="true"
          />
        </li>
      ))}
    </ul>
  );
};
export default TodoList;
